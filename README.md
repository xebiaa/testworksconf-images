# Testworks conference image
Workshop material TestWorksConf

Prerequisites

- [Vagrant](https://www.vagrantup.com/) + [VirtualBox](https://www.virtualbox.org/)

Given:

    $ git clone https://github.com/xebia/testworksconf-images.git 
    $ cd testworksconf-images

When:

    $ vagrant up

Then:

    start building!
