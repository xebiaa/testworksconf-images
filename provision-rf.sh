# Set the app we're provisioning
export currentApp=RobotFramework


#!/usr/bin/env bash
set -e

# Package Versions
PYTHON_VERSION=2.7.10

echo "Start provisioning $currentApp..."
sudo apt-get update > /dev/null 2>&1
sudo apt-get install -y python-pip > /dev/null 2>&1
sudo apt-get install -y python-wxgtk2.8 > /dev/null 2>&1

sudo pip install robotframework > /dev/null 2>&1
sudo pip install robotframework-ride==1.5a2 > /dev/null 2>&1
sudo pip install robotframework-selenium2library > /dev/null 2>&1
sudo pip install WebTest > /dev/null 2>&1
sudo pip install livetest > /dev/null 2>&1
sudo pip install --upgrade robotframework-httplibrary > /dev/null 2>&1
sudo pip install requests > /dev/null 2>&1
sudo pip install -U robotframework-requests > /dev/null 2>&1
sudo pip install robotframework-selenium2library > /dev/null 2>&1
sudo pip install robotframework-sudslibrary > /dev/null 2>&1
sudo pip install paramiko > /dev/null 2>&1
sudo pip install robotframework-sshlibrary > /dev/null 2>&1


# This sets the $HOME/bin dir in the path, overriding the prior check that is done in .profile if /bin even exists for the user. Should be fixed to be more elegant.
echo "PATH=${PATH}:/home/vagrant/bin:/home/vagrant/rf_demo_code/Test_libraries/Java_libraries/Hello_world/java_keywords/bin:/home/vagrant/rf_demo_code/Test_libraries/Java_libraries/Java_stack/robot/execution/lib:/home/vagrant/rf_demo_code/Test_libraries/Swing_libraries/Test_libs:/home/vagrant/rf_demo_code/Test_libraries/_Spec_files" >> /home/vagrant/.pam_environment
. /home/vagrant/.pam_environment


echo "Done provisioning $currentApp!"
