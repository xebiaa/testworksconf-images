# Set the app we're provisioning
export currentApp=Mox

#!/usr/bin/env bash
set -e

echo "Start provisioning $currentApp..."
echo I am provisioning Mox and all the stuff needed for the demo

# install atom
echo "[$currentApp] Installing atom"
sudo add-apt-repository ppa:webupd8team/atom > /dev/null 2>&1
sudo apt-get update > /dev/null 2>&1
sudo apt-get install -y atom > /dev/null 2>&1
#sudo apm install atom-beautify
sudo apm install atom-jasmine > /dev/null 2>&1
sudo apm install atom-wallaby > /dev/null 2>&1
sudo apm install linter > /dev/null 2>&1
sudo apm install linter-jshint > /dev/null 2>&1

# install Grunt
echo "[$currentApp] Installing Grunt"
sudo npm install -g grunt-cli > /dev/null 2>&1

# clone Mox, example project, and SkippyJS git repository
echo "[$currentApp] Cloning git repo"
git clone https://github.com/fvanwijk/mox.git /home/vagrant/repoBase/mox > /dev/null 2>&1
git clone https://github.com/xebia/xsd-open-kitchen.git /home/vagrant/repoBase/xsd-open-kitchen > /dev/null 2>&1
git clone https://github.com/AlbertBrand/skippyjs.git /home/vagrant/repoBase/skippyjs > /dev/null 2>&1

sudo chown -R vagrant:vagrant /home/vagrant/repoBase

cd /home/vagrant/repoBase/mox
echo "[$currentApp] Running Bower install to download dependencies"
bower --allow-root --config.analytics=false install > /dev/null 2>&1
echo "[$currentApp] Running Npm install to download dependencies"
npm install > /dev/null 2>&1
echo "Done provisioning $currentApp!"
