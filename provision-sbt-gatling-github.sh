# Set the app we're provisioning
export currentApp=Gatling

#!/usr/bin/env bash
set -e

echo "Start provisioning $currentApp..."

# install SBT
echo "[$currentApp] Installing SBT"
sudo curl -s https://raw.githubusercontent.com/paulp/sbt-extras/master/sbt > /home/vagrant/bin/sbt && sudo chmod 0755 /home/vagrant/bin/sbt > /dev/null 2>&1
wget https://repo.typesafe.com/typesafe/ivy-releases/org.scala-sbt/sbt-launch/0.13.8/sbt-launch.jar > /home/vagrant/bin/sbt-launch.jar > /dev/null 2>&1

# clone Gatling git repository
echo "[$currentApp] Cloning git repo"
git clone https://github.com/gatling/gatling-sbt-plugin-demo.git /home/vagrant/repoBase/gatling-sbt-plugin-demo > /dev/null 2>&1
#sudo chown -R vagrant:vagrant /home/vagrant/repoBase/gatling-sbt-plugin-demo

# run sbt to download all kinds of stuff
echo "[$currentApp] Running SBT to download dependencies"
cd /home/vagrant/repoBase/gatling-sbt-plugin-demo
/home/vagrant/bin/sbt update > /dev/null 2>&1
echo "Done provisioning $currentApp!"
