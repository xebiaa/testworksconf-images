# Set the app we're provisioning
export currentApp=RobotFramework


#!/usr/bin/env bash
set -e

# Package Versions
PYTHON_VERSION=2.7.10

echo "Start updating PATH variable for $currentApp..."

# This sets the $HOME/bin dir in the path, overriding the prior check that is done in .profile if /bin even exists for the user. Should be fixed to be more elegant.
echo "PATH=${PATH}:/home/vagrant/bin:/home/vagrant/rf_demo_code/Test_libraries/Java_libraries/Hello_world/java_keywords/bin:/home/vagrant/rf_demo_code/Test_libraries/Java_libraries/Java_stack/robot/execution/lib:/home/vagrant/rf_demo_code/Test_libraries/Swing_libraries/Test_libs:/home/vagrant/rf_demo_code/Test_libraries/_Spec_files" >> /home/vagrant/.pam_environment
. /home/vagrant/.pam_environment

echo "Done updating PATH variable for $currentApp!"
