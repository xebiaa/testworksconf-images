# Set the app we're provisioning
export currentApp=Serenity

#!/usr/bin/env bash
set -e

echo "Start provisioning $currentApp..."
# clone the repository
echo "[$currentApp] Cloning git repo"
git clone https://github.com/serenity-bdd/serenity-demos /home/vagrant/repoBase/serenity > /dev/null 2>&1
echo "[$currentApp] Running mvn install to download dependencies"
cd /home/vagrant/repoBase/serenity/cucumber-webtests/
mvn install -DskipTests=true > /dev/null 2>&1
#cd /home/vagrant/repoBase/serenity/jbehave-webtests/
#mvn install -DskipTests=true > /dev/null 2>&1
#cd /home/vagrant/repoBase/serenity/junit-webtests/
#mvn install -DskipTests=true > /dev/null 2>&1

echo "Done provisioning $currentApp!"

