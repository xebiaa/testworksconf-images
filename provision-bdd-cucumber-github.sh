# Set the app we're provisioning
export currentApp=ProtractorCucumber

#!/usr/bin/env bash
set -e

echo "Start provisioning $currentApp..."

# clone the repository

echo "[$currentApp] Cloning git repo"
git clone https://github.com/xebia/testworksconf-protractor-cucumber-workshop.git /home/vagrant/repoBase/testworksconf-protractor-cucumber-workshop > /dev/null 2>&1

sudo chown -R vagrant:vagrant /home/vagrant/repoBase

cd /home/vagrant/repoBase/testworksconf-protractor-cucumber-workshop
echo "[$currentApp] Running Npm install to download dependencies"
npm install > /dev/null 2>&1
echo "[$currentApp] Running Bower install to download dependencies"
bower --allow-root --config.analytics=false install > /dev/null 2>&1
echo "Done provisioning $currentApp!"

