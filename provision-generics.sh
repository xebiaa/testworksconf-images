#!/usr/bin/env bash
set -e

echo "Provisioning generics, this will take a while..."
sudo apt-get update > /dev/null 2>&1
sudo apt-get install -y git > /dev/null 2>&1

# download and unpack IntelliJ IDEA
echo "Provisioning IntelliJ IDEA..."

wget -O /tmp/intellij.tar.gz http://download.jetbrains.com/idea/ideaIC-14.1.4.tar.gz > /dev/null 2>&1
tar -zxf /tmp/intellij.tar.gz -C /home/vagrant > /dev/null 2>&1 &&
cd /home/vagrant/idea-IC-141.1532.4/bin

# create some kind of desktop entry

echo "[Desktop Entry]
Version=1.0
Type=Application
Name=IntelliJ IDEA Community Edition
Icon=/home/vagrant/idea-IC-141.1532.4/bin/idea.png
Exec="/home/vagrant/idea-IC-141.1532.4/bin/idea.sh" %f
Comment=Develop with pleasure!
Categories=Development;IDE;
Terminal=false
StartupWMClass=jetbrains-idea-ce" > /home/vagrant/Desktop/idea.desktop

sudo chown vagrant /home/vagrant/Desktop/idea.desktop
sudo chmod +x /home/vagrant/Desktop/idea.desktop


# install JDK as it was meant to be - from scratch
echo "Provisioning Java JDK..."
mkdir /home/vagrant/java
cd /home/vagrant/java
curl -L --cookie "oraclelicense=accept-securebackup-cookie" http://download.oracle.com/otn-pub/java/jdk/8u60-b27/jdk-8u60-linux-x64.tar.gz -o jdk-8-linux-x64.tar.gz > /dev/null 2>&1

tar -zxf jdk-8-linux-x64.tar.gz > /dev/null 2>&1

sudo mkdir -p /usr/lib/jvm
sudo mv ./jdk1.8.* /usr/lib/jvm/

# register Java
sudo update-alternatives --install "/usr/bin/java" "java" "/usr/lib/jvm/jdk1.8.0_60/bin/java" 1 > /dev/null 2>&1
sudo update-alternatives --install "/usr/bin/javac" "javac" "/usr/lib/jvm/jdk1.8.0_60/bin/javac" 1 > /dev/null 2>&1
sudo update-alternatives --install "/usr/bin/javaws" "javaws" "/usr/lib/jvm/jdk1.8.0_60/bin/javaws" 1 > /dev/null 2>&1

sudo chmod a+x /usr/bin/java  
sudo chmod a+x /usr/bin/javac 
sudo chmod a+x /usr/bin/javaws
sudo chown -R root:root /usr/lib/jvm/jdk1.8.0_60

sudo echo -e "export JAVA_HOME=/usr/lib/jvm/jdk1.8.0_60" >> /etc/environment

# install maven
echo "Provisioning Maven..."
sudo apt-get purge -y maven > /dev/null 2>&1
if ! [ -e /tmp/apache-maven-3.3.3-bin.tar.gz ]; then
  (cd /tmp; curl -OL http://mirror.olnevhost.net/pub/apache/maven/maven-3/3.3.3/binaries/apache-maven-3.3.3-bin.tar.gz > /dev/null 2>&1);
fi

sudo tar -zxf /tmp/apache-maven-3.3.3-bin.tar.gz -C /usr/local/
sudo ln -s /usr/local/apache-maven-3.3.3/bin/mvn /usr/bin/mvn
echo "Maven is on version `mvn -v`"

# install chromium for Axini and for Galen
echo "Provisioning Chromium..."
sudo apt-get -y install chromium-browser > /dev/null 2>&1

# install chromedriver
echo "Provisioning Chromedriver..."
mkdir -p /home/vagrant/chromedriver

wget -N http://chromedriver.storage.googleapis.com/2.10/chromedriver_linux64.zip -P /home/vagrant/chromedriver > /dev/null 2>&1
unzip /home/vagrant/chromedriver/chromedriver_linux64.zip -d /home/vagrant/chromedriver > /dev/null 2>&1
chmod +x /home/vagrant/chromedriver/chromedriver
sudo mv -f /home/vagrant/chromedriver/chromedriver /usr/local/share/chromedriver
sudo ln -s /usr/local/share/chromedriver /usr/local/bin/chromedriver
sudo ln -s /usr/local/share/chromedriver /usr/bin/chromedriver

# install npm -- Guillaume, Edze, Marc
echo "Provisioning npm..."
curl -sL https://deb.nodesource.com/setup_0.12 | sudo bash - > /dev/null 2>&1
sudo apt-get -y install nodejs > /dev/null 2>&1

# install Grunt and Bower and Gulp
echo "Provisioning Grunt, Bower and Gulp..."
sudo npm install -g grunt-cli > /dev/null 2>&1
sudo npm install -g bower > /dev/null 2>&1
sudo npm install -g gulp > /dev/null 2>&1

# create and set path to include own 'bin' directory
echo "Creating bin dir in HOME and updating PATH..."
mkdir -p /home/vagrant/bin
echo "PATH=${PATH}:/home/vagrant/bin" >> /home/vagrant/.pam_environment
. /home/vagrant/.pam_environment
chown vagrant:vagrant /home/vagrant/.pam_environment
# echo -e "export PATH=$PATH:/home/vagrant/bin" >> /home/vagrant/.bashrc

# create base directory into which all github repositories will be cloned
echo "Creating repo base dir..."
mkdir /home/vagrant/repoBase

sudo chown -R vagrant:vagrant /home/vagrant/repoBase
sudo chown -R vagrant:vagrant /home/vagrant/bin
