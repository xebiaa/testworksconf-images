# Set the app we're provisioning
export currentApp=FitNesse

#!/usr/bin/env bash
set -e

echo "Start provisioning $currentApp..."

# download and unpack required IntelliJ Plugins
echo "[$currentApp] Installing IntelliJ IDEA plugins"
mkdir -p /home/vagrant/.IdeaIC14/config/plugins
cd /home/vagrant/.IdeaIC14/config/plugins
wget https://github.com/gshakhn/idea-fitnesse/releases/download/1.0.4/idea-fitnesse-1.0.4.zip > /dev/null 2>&1
wget https://github.com/downloads/kumaraman21/IntelliJBehave/IntelliJBehave-1.2.zip > /dev/null 2>&1
unzip idea-fitnesse-1.0.4.zip > /dev/null 2>&1
unzip IntelliJBehave-1.2.zip > /dev/null 2>&1
#sudo chown -R vagrant:vagrant /home/vagrant/.IdeaIC14

# clone Arjan Molenaar's repository
echo "[$currentApp] Cloning git repo"
cd /home/vagrant/repoBase
git clone https://github.com/amolenaar/beefing-up-fitnesse.git /home/vagrant/repoBase/beefing-up-fitnesse > /dev/null 2>&1
#sudo chown -R vagrant:vagrant /home/vagrant/repoBase/beefing-up-fitnesse

# set up IntelliJ project files
echo "[$currentApp] Setting up IntelliJ project files"
cd /home/vagrant/repoBase/beefing-up-fitnesse
./gradlew idea > /dev/null 2>&1

# starting the IDE yields a GUI error
# ./idea.sh

echo "Done provisioning $currentApp!"
