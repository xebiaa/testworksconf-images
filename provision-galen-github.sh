# Set the app we're provisioning
export currentApp=Galen

#!/usr/bin/env bash
set -e

echo "Start provisioning $currentApp..."

# install Galen framework
echo "[$currentApp] Running Npm install to download dependencies"
sudo npm install -g galenframework-cli > /dev/null 2>&1

# clone git repository
echo "[$currentApp] Cloning git repo"
git clone https://github.com/galenframework/galen.git /home/vagrant/repoBase/galen > /dev/null 2>&1
sudo chown -R vagrant:vagrant /home/vagrant/repoBase/galen
echo "Done provisioning $currentApp!"

