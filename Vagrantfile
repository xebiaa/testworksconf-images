Vagrant.configure(2) do |config|
  config.vm.box = "box-cutter/ubuntu1404-desktop"

  config.vm.provider "virtualbox" do |vb|
  	# Display the VirtualBox GUI when booting the machine
  	vb.gui = true
  	# Customize the amount of memory on the VM:
  	vb.memory = "2048"
  	vb.cpus = 2
  end

  # provision generics: git, npm, chromedriver, etc
  config.vm.provision 'shell' do |s|
    s.path = 'provision-generics.sh'
    s.privileged = true
  end

  # provision Robot Framework
  config.vm.provision 'shell' do |s|
    s.path = 'provision-rf.sh'
    s.privileged = true
  end

  # provision Robot Framework PATH
  config.vm.provision 'shell' do |s|
    s.path = 'provision-rf-path.sh'
    s.privileged = false
  end

  # Beefing up FitNesse
  # provision IntelliJ CE and JDK
  config.vm.provision 'shell' do |s|
    s.path = 'provision-IDEA-fitnesse-github.sh'
    s.privileged = false
  end

  # Galen
  config.vm.provision 'shell' do |s|
    s.path = 'provision-galen-github.sh'
    s.privileged = true
  end

  # Gatling
  config.vm.provision 'shell' do |s|
    s.path = 'provision-sbt-gatling-github.sh'
    s.privileged = false
  end

  # Mox and the rest
  config.vm.provision 'shell' do |s|
    s.path = 'provision-mox.sh'
    s.privileged = true
  end

  # BDD / Cucumber install
  config.vm.provision 'shell' do |s|
    s.path = 'provision-bdd-cucumber-github.sh'
    s.privileged = false
  end

  # Serenity install
  config.vm.provision 'shell' do |s|
    s.path = 'provision-serenity-github.sh'
    s.privileged = false
  end

    # Set wallpaper image
  config.vm.provision 'file' do |f| 
    f.source = 'xebia.png' 
    f.destination = '/home/vagrant/xebia.png'
  end

   config.vm.provision 'file' do |f| 
    f.source = '10_unity_greeter_background.gschema.override' 
    f.destination = '/home/vagrant/10_unity_greeter_background.gschema.override'
  end

  config.vm.provision 'shell' do |s|
    s.path = 'provision-wallpaper-lockscreen.sh'
    s.privileged = false
  end

  config.vm.provision 'shell' do |s|
    s.path = 'provision-wallpaper-user.sh'
    s.privileged = false
  end

  # Kill user session to force load of PATH env var
  config.vm.provision 'shell' do |s|
    s.inline = "pkill -u vagrant"
  end

end
